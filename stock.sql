-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2015 at 09:23 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stock`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill_table`
--

CREATE TABLE IF NOT EXISTS `bill_table` (
`id` int(10) unsigned NOT NULL,
  `bill` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `chalan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `shop` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `product` text COLLATE utf8_unicode_ci NOT NULL,
  `piece` text COLLATE utf8_unicode_ci NOT NULL,
  `size` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bill_table`
--

INSERT INTO `bill_table` (`id`, `bill`, `chalan`, `shop`, `date`, `product`, `piece`, `size`, `created_at`, `updated_at`) VALUES
(1, '023', '1406', NULL, '2015-03-03', '4d010(Floor)<br>', '40<br>', '160<br>', '2015-04-15 07:26:15', '2015-04-15 07:26:15'),
(2, '1', '3741', NULL, '2015-02-11', 'QGPG602(Floor)<br>', '44<br>', '176<br>', '2015-04-15 07:43:56', '2015-04-15 07:43:56'),
(3, '2', '3753', NULL, '2015-02-16', 'QGPG602(Floor)<br>', '400<br>', '1600<br>', '2015-04-15 07:44:58', '2015-04-15 07:44:58'),
(4, '3', '3754', NULL, '2015-02-16', 'QGPG602(Floor)<br>', '8<br>', '32<br>', '2015-04-15 07:47:08', '2015-04-15 07:47:08'),
(5, '6', '143', NULL, '2015-02-12 00:00:00', 'QGPG602(Floor)<br>', '275<br>', '1100<br>', '2015-04-15 07:48:29', '2015-04-15 07:48:29'),
(6, '6', '144', NULL, '2015-02-12 00:00:00', 'QGPG602(Floor)<br>', '8<br>', '32<br>', '2015-04-15 07:49:18', '2015-04-15 07:49:18'),
(7, '4', '4970', NULL, '2014-04-13 00:00:00', '6038(Floor)<br>', '4<br>', '16<br>', '2015-04-15 08:13:42', '2015-04-15 08:13:42'),
(8, '33', '496', NULL, '2014-06-23 00:00:00', '6038(Floor)<br>', '305<br>', '1220<br>', '2015-04-15 08:15:23', '2015-04-15 08:15:23'),
(9, '88', '1041', NULL, '2014-11-12 00:00:00', '6038(Floor)<br>', '183<br>', '732<br>', '2015-04-15 08:16:42', '2015-04-15 08:16:42'),
(10, '99', '1366', NULL, '2015-02-10 00:00:00', '6038(Floor)<br>', '3<br>', '12<br>', '2015-04-15 08:17:42', '2015-04-15 08:17:42'),
(11, '1', '148', NULL, '2015-02-14 00:00:00', '4D033(Floor)<br>', '52<br>', '208<br>', '2015-04-16 03:56:58', '2015-04-16 03:56:58'),
(12, '2', '1398', NULL, '2015-02-28 00:00:00', '4D033(Floor)<br>', '224<br>', '896<br>', '2015-04-16 03:58:06', '2015-04-16 03:58:06'),
(13, '3', '1405', NULL, '2015-03-04 00:00:00', '4D033(Floor)<br>', '276<br>', '1104<br>', '2015-04-16 03:58:58', '2015-04-16 03:58:58'),
(14, '2', '3', NULL, '2015-04-16 00:00:00', '4D033(Floor)<br>', '50<br>', '200<br>', '2015-04-16 07:40:11', '2015-04-16 07:40:11'),
(15, '1', '1', NULL, '2015-04-16 00:00:00', 'H360020(Light)<br>', '100<br>', '200<br>', '2015-04-16 08:37:30', '2015-04-16 08:37:30'),
(16, '333', '33', NULL, '2015-04-16 00:00:00', '4d010(Floor)<br>', '100<br>', '400<br>', '2015-04-16 17:00:23', '2015-04-16 17:00:23'),
(17, '1111', '111', NULL, '2015-04-16 00:00:00', '4D033(Floor)<br>', '100<br>', '400<br>', '2015-04-16 17:02:50', '2015-04-16 17:02:50'),
(18, '24234234', '234236435', 'b', '2015-04-25 00:00:00', '4d010(Floor)<br>', '10<br>', '40<br>', '2015-04-25 13:37:45', '2015-04-25 13:37:45'),
(19, '4323423', '5433223', 'shop3', '2015-04-25 00:00:00', '4d010(Floor)<br>', '2<br>', '8<br>', '2015-04-25 13:43:32', '2015-04-25 13:43:32');

-- --------------------------------------------------------

--
-- Table structure for table `lc_table`
--

CREATE TABLE IF NOT EXISTS `lc_table` (
`id` int(10) unsigned NOT NULL,
  `lc` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `product` text COLLATE utf8_unicode_ci NOT NULL,
  `piece` text COLLATE utf8_unicode_ci NOT NULL,
  `size` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lc_table`
--

INSERT INTO `lc_table` (`id`, `lc`, `date`, `product`, `piece`, `size`, `created_at`, `updated_at`) VALUES
(1, '0222', '2015-03-15 00:00:00', '4d010<br>', '1600<br>', '24X24<br>', '2015-04-15 07:24:39', '2015-04-15 07:24:39'),
(2, '0222', '2015-04-15 00:00:00', 'fpiE26303<br>', '4500<br>', '9.5X26<br>', '2015-04-15 07:35:35', '2015-04-15 07:35:35'),
(3, '123456789', '2015-03-15 00:00:00', 'QGPG602<br>', '3264<br>', '24X24<br>', '2015-04-15 07:42:26', '2015-04-15 07:42:26'),
(4, '0225555', '2015-02-01 00:00:00', '6038<br>', '520<br>', '24X24<br>', '2015-04-15 08:11:15', '2015-04-15 08:11:15'),
(5, '0255555', '2015-02-12 00:00:00', '4D033<br>', '3584<br>', '24X24<br>', '2015-04-16 03:54:16', '2015-04-16 03:54:16'),
(6, 'B/F', '2015-04-16 00:00:00', 'F306025<br>', '2400<br>', '300X600<br>', '2015-04-16 07:53:14', '2015-04-16 07:53:14'),
(7, 'B/F', '2015-04-16 00:00:00', 'F306025<br>', '2400<br>', '300X600<br>', '2015-04-16 07:54:50', '2015-04-16 07:54:50'),
(8, 'B/F', '2015-04-16 00:00:00', 'F306026<br>', '2400<br>', '300X600<br>', '2015-04-16 07:55:13', '2015-04-16 07:55:13'),
(9, 'B/F', '2015-04-16 00:00:00', 'F306025<br>', '800<br>', '300X600<br>', '2015-04-16 07:55:56', '2015-04-16 07:55:56'),
(10, 'B/F', '2015-04-16 00:00:00', 'F306026<br>', '1800<br>', '300X300<br>', '2015-04-16 07:56:46', '2015-04-16 07:56:46'),
(11, '1', '0000-00-00 00:00:00', 'Hsp63020<br>', '6400<br>', '12X24<br>', '2015-04-16 08:17:01', '2015-04-16 08:17:01'),
(12, '2', '2015-04-16 00:00:00', 'H360020<br>', '4000<br>', '12X24<br>', '2015-04-16 08:35:52', '2015-04-16 08:35:52'),
(13, '1090', '2015-05-04', '111-A<br>', '100<br>', '12X12<br>', '2015-05-04 07:03:27', '2015-05-04 07:03:27');

-- --------------------------------------------------------

--
-- Table structure for table `product_table`
--

CREATE TABLE IF NOT EXISTS `product_table` (
`id` int(10) unsigned NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mother_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `total_in` int(11) NOT NULL,
  `total_out` int(11) NOT NULL DEFAULT '0',
  `wastage` int(11) DEFAULT '0',
  `sample` int(11) NOT NULL DEFAULT '0',
  `returns` int(11) NOT NULL DEFAULT '0',
  `unit_product_size_first` double(12,2) NOT NULL,
  `unit_product_size_second` double(12,2) NOT NULL,
  `unit_size` double(12,2) NOT NULL,
  `dimension` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `now_piece` int(11) NOT NULL DEFAULT '0',
  `now_sft` double(12,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_table`
--

INSERT INTO `product_table` (`id`, `code`, `mother_code`, `total_in`, `total_out`, `wastage`, `sample`, `returns`, `unit_product_size_first`, `unit_product_size_second`, `unit_size`, `dimension`, `now_piece`, `now_sft`, `created_at`, `updated_at`) VALUES
(1, '4d010(Floor)', '4d010', 1600, 42, 40, 0, 110, 24.00, 24.00, 4.00, '24X24', 1518, 6072.00, '2015-04-25 13:43:32', '2015-04-25 13:43:32'),
(2, 'fpiE26303(Light)', 'fpiE26303', 4500, 0, 50, 0, 0, 9.50, 26.00, 1.72, '9.5X26', 4450, 7632.99, '2015-04-15 07:35:35', '0000-00-00 00:00:00'),
(3, 'QGPG602(Floor)', 'QGPG602', 3264, 735, 33, 2, 0, 24.00, 24.00, 4.00, '24X24', 2499, 9996.00, '2015-04-15 13:39:56', '2015-04-15 13:39:56'),
(4, '6038(Floor)', '6038', 520, 495, 16, 0, 0, 24.00, 24.00, 4.00, '24X24', 9, 2016.00, '2015-04-15 08:17:43', '2015-04-15 08:17:43'),
(5, '4D033(Floor)', '4D033', 3584, 702, 40, 0, 0, 24.00, 24.00, 4.00, '24X24', 2842, 11368.00, '2015-04-16 17:02:50', '2015-04-16 17:02:50'),
(6, 'F306025(Light)', 'F306025', 4800, 0, 80, 0, 0, 300.00, 600.00, 1250.00, '300X600', 4720, 5900000.00, '2015-04-16 07:54:50', '2015-04-16 07:54:50'),
(7, 'F306026(Deep)', 'F306026', 2400, 0, 40, 0, 0, 300.00, 600.00, 1250.00, '300X600', 2360, 2950000.00, '2015-04-16 07:55:13', '0000-00-00 00:00:00'),
(8, 'F306025(Decor)', 'F306025', 800, 0, 10, 0, 0, 300.00, 600.00, 1250.00, '300X600', 790, 987500.00, '2015-04-16 07:55:56', '0000-00-00 00:00:00'),
(9, 'F306026(Floor)', 'F306026', 1800, 0, 45, 0, 0, 300.00, 300.00, 625.00, '300X300', 1755, 1096875.00, '2015-04-16 07:56:46', '0000-00-00 00:00:00'),
(10, 'Hsp63020(Light)', 'Hsp63020', 6400, 0, 100, 0, 0, 12.00, 24.00, 2.00, '12X24', 6300, 12600.00, '2015-04-16 08:17:01', '0000-00-00 00:00:00'),
(11, 'H360020(Light)', 'H360020', 4000, 100, 50, 0, 0, 12.00, 24.00, 2.00, '12X24', 3850, 7900.00, '2015-04-16 08:37:30', '2015-04-16 08:37:30'),
(12, '111-A(Light)', '111-A', 100, 0, 0, 0, 0, 12.00, 12.00, 1.00, '12X12', 100, 100.00, '2015-05-04 07:03:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stock_in`
--

CREATE TABLE IF NOT EXISTS `stock_in` (
`id` int(10) unsigned NOT NULL,
  `lc` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_code_first` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_code_second` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `piece` int(11) NOT NULL,
  `wastage_piece` int(11) NOT NULL DEFAULT '0',
  `unit_product_size` double(12,2) NOT NULL,
  `unit_product_size_first` double(12,2) NOT NULL,
  `unit_product_size_second` double(12,2) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stock_in`
--

INSERT INTO `stock_in` (`id`, `lc`, `product_code`, `product_code_first`, `product_code_second`, `piece`, `wastage_piece`, `unit_product_size`, `unit_product_size_first`, `unit_product_size_second`, `date`, `created_at`, `updated_at`) VALUES
(1, '0222', '4d010(Floor)', '4d010', 'Floor', 1600, 40, 4.00, 24.00, 24.00, '2015-03-14 18:00:00', '2015-04-15 07:24:39', '2015-04-15 07:24:39'),
(2, '0222', 'fpiE26303(Light)', 'fpiE26303', 'Light', 4500, 50, 1.72, 9.50, 26.00, '2015-04-14 18:00:00', '2015-04-15 07:35:35', '2015-04-15 07:35:35'),
(3, '123456789', 'QGPG602(Floor)', 'QGPG602', 'Floor', 3264, 28, 4.00, 24.00, 24.00, '2015-03-14 18:00:00', '2015-04-15 07:42:26', '2015-04-15 07:42:26'),
(4, '0225555', '6038(Floor)', '6038', 'Floor', 520, 16, 4.00, 24.00, 24.00, '2015-01-31 18:00:00', '2015-04-15 08:11:15', '2015-04-15 08:11:15'),
(5, '0255555', '4D033(Floor)', '4D033', 'Floor', 3584, 40, 4.00, 24.00, 24.00, '2015-02-11 18:00:00', '2015-04-16 03:54:15', '2015-04-16 03:54:15'),
(6, 'B/F', 'F306025(Light)', 'F306025', 'Light', 2400, 40, 1250.00, 300.00, 600.00, '2015-04-15 18:00:00', '2015-04-16 07:53:14', '2015-04-16 07:53:14'),
(7, 'B/F', 'F306025(Light)', 'F306025', 'Light', 2400, 40, 1250.00, 300.00, 600.00, '2015-04-15 18:00:00', '2015-04-16 07:54:50', '2015-04-16 07:54:50'),
(8, 'B/F', 'F306026(Deep)', 'F306026', 'Deep', 2400, 40, 1250.00, 300.00, 600.00, '2015-04-15 18:00:00', '2015-04-16 07:55:13', '2015-04-16 07:55:13'),
(9, 'B/F', 'F306025(Decor)', 'F306025', 'Decor', 800, 10, 1250.00, 300.00, 600.00, '2015-04-15 18:00:00', '2015-04-16 07:55:56', '2015-04-16 07:55:56'),
(10, 'B/F', 'F306026(Floor)', 'F306026', 'Floor', 1800, 45, 625.00, 300.00, 300.00, '2015-04-15 18:00:00', '2015-04-16 07:56:46', '2015-04-16 07:56:46'),
(11, '1', 'Hsp63020(Light)', 'Hsp63020', 'Light', 6400, 100, 2.00, 12.00, 24.00, '0000-00-00 00:00:00', '2015-04-16 08:17:01', '2015-04-16 08:17:01'),
(12, '2', 'H360020(Light)', 'H360020', 'Light', 4000, 50, 2.00, 12.00, 24.00, '2015-04-15 18:00:00', '2015-04-16 08:35:52', '2015-04-16 08:35:52'),
(13, '1090', '111-A(Light)', '111-A', 'Light', 100, 0, 1.00, 12.00, 12.00, '2015-05-03 18:00:00', '2015-05-04 07:03:27', '2015-05-04 07:03:27');

-- --------------------------------------------------------

--
-- Table structure for table `stock_out`
--

CREATE TABLE IF NOT EXISTS `stock_out` (
`id` int(10) unsigned NOT NULL,
  `bill` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `chalan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sold_sft` double(12,2) DEFAULT NULL,
  `sold_piece` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stock_out`
--

INSERT INTO `stock_out` (`id`, `bill`, `chalan`, `product_code`, `sold_sft`, `sold_piece`, `date`, `created_at`, `updated_at`) VALUES
(1, '023', '1406', '4d010(Floor)', 160.00, 40, '2015-03-02 18:00:00', '2015-04-15 07:26:15', '2015-04-15 07:26:15'),
(2, '1', '3741', 'QGPG602(Floor)', 176.00, 44, '2015-02-10 18:00:00', '2015-04-15 07:43:56', '2015-04-15 07:43:56'),
(3, '2', '3753', 'QGPG602(Floor)', 1600.00, 400, '2015-02-15 18:00:00', '2015-04-15 07:44:58', '2015-04-15 07:44:58'),
(4, '3', '3754', 'QGPG602(Floor)', 32.00, 8, '2015-02-15 18:00:00', '2015-04-15 07:47:08', '2015-04-15 07:47:08'),
(5, '6', '143', 'QGPG602(Floor)', 1100.00, 275, '2015-02-11 18:00:00', '2015-04-15 07:48:29', '2015-04-15 07:48:29'),
(6, '6', '144', 'QGPG602(Floor)', 32.00, 8, '2015-02-11 18:00:00', '2015-04-15 07:49:18', '2015-04-15 07:49:18'),
(7, '4', '4970', '6038(Floor)', 16.00, 4, '2014-04-12 18:00:00', '2015-04-15 08:13:42', '2015-04-15 08:13:42'),
(8, '33', '496', '6038(Floor)', 1220.00, 305, '2014-06-22 18:00:00', '2015-04-15 08:15:23', '2015-04-15 08:15:23'),
(9, '88', '1041', '6038(Floor)', 732.00, 183, '2014-11-11 18:00:00', '2015-04-15 08:16:42', '2015-04-15 08:16:42'),
(10, '99', '1366', '6038(Floor)', 12.00, 3, '2015-02-09 18:00:00', '2015-04-15 08:17:42', '2015-04-15 08:17:42'),
(11, '1', '148', '4D033(Floor)', 208.00, 52, '2015-02-13 18:00:00', '2015-04-16 03:56:58', '2015-04-16 03:56:58'),
(12, '2', '1398', '4D033(Floor)', 896.00, 224, '2015-02-27 18:00:00', '2015-04-16 03:58:06', '2015-04-16 03:58:06'),
(13, '3', '1405', '4D033(Floor)', 1104.00, 276, '2015-03-03 18:00:00', '2015-04-16 03:58:58', '2015-04-16 03:58:58'),
(14, '2', '3', '4D033(Floor)', 200.00, 50, '2015-04-15 18:00:00', '2015-04-16 07:40:11', '2015-04-16 07:40:11'),
(15, '1', '1', 'H360020(Light)', 200.00, 100, '2015-04-15 18:00:00', '2015-04-16 08:37:30', '2015-04-16 08:37:30'),
(16, '333', '33', '4d010(Floor)', 400.00, 100, '2015-04-15 18:00:00', '2015-04-16 17:00:23', '2015-04-16 17:00:23'),
(17, '1111', '111', '4D033(Floor)', 400.00, 100, '2015-04-15 18:00:00', '2015-04-16 17:02:50', '2015-04-16 17:02:50'),
(18, '24234234', '234236435', '4d010(Floor)', 40.00, 10, '2015-04-24 18:00:00', '2015-04-25 13:37:45', '2015-04-25 13:37:45'),
(19, '4323423', '5433223', '4d010(Floor)', 8.00, 2, '2015-04-24 18:00:00', '2015-04-25 13:43:32', '2015-04-25 13:43:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `remember_token`, `password`, `created_at`, `updated_at`) VALUES
(1, 'megaminds', 'udCkHw4m6h4TaLjJYhr36NfuA7KcqMFHASlStE9RJHVRExhjEiQEeOt9C31j', '$2y$10$MIINMl1PHLCAQIjKgQ0yn.OhkEOKW.KAOByyol6Xd7f/lb8aQUhVi', '2015-04-13 13:24:41', '2015-04-13 13:24:41');

-- --------------------------------------------------------

--
-- Table structure for table `wastage_sample_return`
--

CREATE TABLE IF NOT EXISTS `wastage_sample_return` (
`id` int(10) unsigned NOT NULL,
  `product_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `piece` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wastage_sample_return`
--

INSERT INTO `wastage_sample_return` (`id`, `product_code`, `piece`, `date`, `type`, `created_at`, `updated_at`) VALUES
(1, 'QGPG602(Floor)', 5, '2015-04-14 18:00:00', 'return', '2015-04-15 13:34:01', '2015-04-15 13:34:01'),
(2, 'QGPG602(Floor)', 2, '2015-04-14 18:00:00', 'sample', '2015-04-15 13:38:21', '2015-04-15 13:38:21'),
(3, 'QGPG602(Floor)', 5, '2015-04-14 18:00:00', 'wastage', '2015-04-15 13:39:56', '2015-04-15 13:39:56'),
(4, '4d010(Floor)', 10, '2015-04-15 18:00:00', 'return', '2015-04-16 17:49:49', '2015-04-16 17:49:49'),
(5, '4d010(Floor)', 100, '2015-04-15 18:00:00', 'return', '2015-04-16 17:51:15', '2015-04-16 17:51:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bill_table`
--
ALTER TABLE `bill_table`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lc_table`
--
ALTER TABLE `lc_table`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_table`
--
ALTER TABLE `product_table`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_in`
--
ALTER TABLE `stock_in`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_out`
--
ALTER TABLE `stock_out`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wastage_sample_return`
--
ALTER TABLE `wastage_sample_return`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bill_table`
--
ALTER TABLE `bill_table`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `lc_table`
--
ALTER TABLE `lc_table`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `product_table`
--
ALTER TABLE `product_table`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `stock_in`
--
ALTER TABLE `stock_in`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `stock_out`
--
ALTER TABLE `stock_out`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wastage_sample_return`
--
ALTER TABLE `wastage_sample_return`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
