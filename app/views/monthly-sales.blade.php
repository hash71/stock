@extends('layouts.default.master')

@section('page-header', "Monthly Stock Sheet")

@section('content')


<!-- MAIN CONTENT STARTS -->
<div class="content-wrapper">
  <div class="row">
      <div class="text-right padA10">
        <button href="#" id="print" class="btn btn-primary medium mrgR20" title="">
        <span class="button-content"><i class="glyph-icon icon-list"></i> Print Monthly Stock Sheet</span>
        </button>
      </div>
    </div>
    <!-- tile body -->
    <section class="content">
      <form action="{{URL::to('stock/monthly')}}" method="post">
      <div class="box box-primary filter-box">
        <div class="box-header">
          <h3 class="box-title">Filter options</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-xs-1 text-right">
              <label for="">Month: </label>
            </div>
            <div class="col-xs-3 form-group">
              <select name="month" id="month" class="form-control">
                <option value="01" <?php if($m==1) echo "selected";?>>January</option>
                <option value="02" <?php if($m==2) echo "selected";?>>February</option>
                <option value="03" <?php if($m==3) echo "selected";?>>March</option>
                <option value="04" <?php if($m==4) echo "selected";?>>April</option>
                <option value="05" <?php if($m==5) echo "selected";?>>May</option>
                <option value="06" <?php if($m==6) echo "selected";?>>June</option>
                <option value="07" <?php if($m==7) echo "selected";?>>July</option>
                <option value="08" <?php if($m==8) echo "selected";?>>August</option>
                <option value="09" <?php if($m==9) echo "selected";?>>September</option>
                <option value="10" <?php if($m==10) echo "selected";?>>October</option>
                <option value="11" <?php if($m==11) echo "selected";?>>November</option>
                <option value="12" <?php if($m==12) echo "selected";?>>December</option>
              </select>
            </div>
            <div class="col-xs-1 text-right">
              <label for="">Year :</label>
            </div>
            <div class="col-xs-2 form-group">
              <select name="year" id="" class="form-control">
                <option value="2015" <?php if($y==2015) echo "selected";?>>2015</option>
                <option value="2016" <?php if($y==2016) echo "selected";?>>2016</option>
                <option value="2017" <?php if($y==2017) echo "selected";?>>2017</option>
                <option value="2018" <?php if($y==2018) echo "selected";?>>2018</option>
                <option value="2019" <?php if($y==2019) echo "selected";?>>2019</option>
                <option value="2020" <?php if($y==2020) echo "selected";?>>2020</option>
              </select>
            </div>

            <div class="col-xs-1">
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          </div>
        </div><!-- /.box-body -->
      </div>
      </form>


      <div class="box">
        <div class="box-body">
          <table class="table table-striped">
            <tbody><tr>

              <th>Product Code</th>
              <th>Size</th>
              <th>Total Product<br>Sale (pieces)</th>
              <th>Total Product<br>Sale (sft)</th>
              <th>Total Product<br>Return (pieces)</th>
              <th>Total Product<br>Return (sft)</th>
            </tr>
            @foreach($datas as $data)
              <tr>
                <td>{{$data->product_code}}</td>
                <td>{{ProductTable::where('code',$data->product_code)->pluck('dimension')}}</td>
                <td>{{$data->sold_piece}}</td>
                <td>{{$data->sold_piece * (ProductTable::where('code',$data->product_code)->pluck('unit_size'))}}</td>

                <td>{{
                  $ret=WSR::where('product_code',$data->product_code)
                    ->where('date','>=',$start)
                    ->where('date','<=',$end)
                    ->where('type','return')
                    ->sum('piece')
                }}</td>

                <td>{{ProductTable::where('code',$data->product_code)->pluck('unit_size')* $ret}}</td>

              </tr>
            @endforeach

          </tbody></table>
        </div><!-- /.box-body -->
      </div>
    </section>
    <!-- /tile body -->
</div>
@stop
@section('custom_script')
@include('print-script')
@stop