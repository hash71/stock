@extends('layouts.default.master')

@section('page-header', "New Product Insert")

@section('content')

<!-- MAIN CONTENT STARTS -->
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <form action="{{URL::to('stock/store-stockin')}}" method="post">
      <div class="box box-primary" style="margin-bottom: 50px;">
        <div class="box-header">
          <h3 class="box-title">LC information</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-xs-1 text-right">
              <label for="">LC No. :</label>
            </div>
            <div class="col-xs-5">
              <input type="text" name="lc" class="form-control" id="lcnumber" value="" required />
            </div>
            <div class="col-xs-1 text-right">
              <label for="">Date :</label>
            </div>
            <div class="col-xs-4">
              <input type="text" class="form-control datepicker" placeholder="YYYY-MM-DD" id="lcdate" name="date" value="{{date("Y-m-d")}}" required />
            </div>
          </div>
        </div><!-- /.box-body -->
      </div>

      <div class="box box-warning single-product mrgB20">
        <div class="box-header">
          <h3 class="box-title product-num">Product 1</h3>
          <a href="#" class="remove btn btn-sm bg-red pull-right tooltip-button mrgR10" data-placement="left" title="" data-original-title="Remove Product" style="margin-top:2px;" id="remove1"><i class="fa fa-remove"></i></a>
        </div>
        <div class="box-body">
          <div class="row mrgB20">
            <div class="col-xs-2 text-right">
              <label for="">Product Code:</label>
            </div>
            <div class="col-xs-2">
              <input type="text" class="form-control" name="product_code_first1" id="code1" value="" required />
            </div>
            <div class="col-xs-2">
              <select class="form-control" name="product_code_second1">
                <option>Light</option>
                <option>Deep</option>
                <option>Floor</option>
                <option>Decor</option>
              </select>
            </div>
            <div class="col-xs-1 text-right">
              <label for="">Size:</label>
            </div>
            <div class="col-xs-2">
              <input type="number" class="form-control" min="0" step="any" name="unit_product_size_first1" id="asize1" value="" required />
            </div>
            <div class="form-input col-md-1" style="padding: 5px 0px; width: 10px;"> X </div>
            <div class="col-xs-2">
              <input type="number" class="form-control" min="0" step="any" name="unit_product_size_second1" id="bsize1" value="" required />
            </div>
          </div>

          <div class="row">
            <div class="col-xs-2 text-right">
              <label for="">Quantity (pieces):</label>
            </div>
            <div class="col-xs-3">
              <input type="number" class="form-control" min="0" name="piece1" id="quantity1" value="" required />
            </div>
            <div class="col-xs-2 text-right">
              <label for="">Wastage (pieces):</label>
            </div>
            <div class="col-xs-2">
              <input type="number" class="form-control" min="0" name="wastage_piece1" id="wastage_piece1" value="0" />
            </div>

          </div>
        </div><!-- /.box-body -->
      </div>

      <div class="row mrgA20">
        <a href="javascript:;" class="btn medium bg-aqua col-md-2" title="" id="newproduct">
          <span class="button-content"><i class="fa fa-plus"></i> Add another product</span>
        </a>
        <div class="form-row col-md-4 col-md-offset-6 ">
          <div class="form-label col-md-8">
            <input type="submit" value="Save All Products" class="btn btn-primary medium wid100">
          </div>

          <a href="{{URL::to('/')}}" class="btn medium bg-gray col-md-4" title="">
            <span class="button-content"> Cancel</span>
          </a>
        </div>
      </div>

    </form>
  </section><!-- /.content -->

</div><!-- /.content-wrapper -->
<!-- MAIN CONTENT ENDS -->
@stop


@section('custom_script')
  @include('add_another_script')
@stop