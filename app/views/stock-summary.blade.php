@extends('layouts.default.master')

@section('page-header', "Stock Summary")

@section('content')


<!-- MAIN CONTENT STARTS -->
<div class="content-wrapper">
    <div class="row">
      <div class="text-right padA10">
        <button href="#" id="print" class="btn btn-primary medium mrgR20" title="">
        <span class="button-content"><i class="glyph-icon icon-list"></i> Print Stock Summary</span>
        </button>
      </div>
    </div>
    <!-- tile body -->
    <section class="content">
      <form action="{{URL::to('stock/summary')}}" method="post">
      <div class="box box-primary filter-box">
        <div class="box-header">
          <h3 class="box-title">Filter options</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-xs-1 text-right">
              <label for="">From: </label>
            </div>
            <div class="col-xs-2 form-group">
              <input type="text" class="form-control datepicker" placeholder="YYYY-MM-DD" name="from" value="{{$start}}">
            </div>

            <div class="col-xs-1 text-right">
              <label for="">To: </label>
            </div>
            <div class="col-xs-2 form-group">
              <input type="text" class="form-control datepicker" placeholder="YYYY-MM-DD" name="to" value="{{$end}}">
            </div>

            <div class="col-xs-1 text-right">
              <label for="">Product:</label>
            </div>
            <div class="col-xs-3 form-group padT5">
              <select name="product" id="product" class="form-control chosen">
                <option value="-1">All Products</option>
                @foreach(ProductTable::distinct('code')->lists('code') as $code)
                  <option value="{{$code}}" <?php if($code == $scode) echo "selected"?> >{{$code}}</option>
                @endforeach
              </select>
            </div>

            <div class="col-xs-1">
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          </div>
        </div><!-- /.box-body -->
      </div>
      </form>


      <div class="box">
        <div class="box-body">
          <table class="table table-striped">
            <tbody><tr>

              <th>Product Code</th>
              <th>Product Size</th>
              <th>Sold (pieces)</th>
              <th>Sold (sft)</th>
            </tr>
            @foreach($datas as $data)

              <tr>
                <td>{{$data->product_code}}</td>
                <td>{{$x = ProductTable::where('code',$data->product_code)->pluck('dimension')}}</td>
                <td>{{$y = $data->sold_piece}}</td>
                <?php $z = ProductTable::where('code',$data->product_code)->pluck('unit_size'); ?>
                <td>{{$z*$y}}</td>
              </tr>

            @endforeach


          </tbody></table>
        </div><!-- /.box-body -->
      </div>
    </section>
    <!-- /tile body -->
</div>
@stop
@section('custom_script')

<script>
  jQuery(document).ready(function($) {

    $('.chosen').chosen();

    //Datepicker
      $('.datepicker').datepicker({
        'format': 'yyyy-mm-dd'
      });

  });



</script>

@include('print-script')
@stop