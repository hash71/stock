@extends('layouts.default.master')

@section('page-header', "Product Return")

@section('content')
<!-- MAIN CONTENT STARTS -->
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <form action = "{{URL::to('products/returns')}}" method="post" onsubmit="return validate();">
      <div class="box box-info mrgB20">
        <div class="box-body">
          <div class="row mrgB20">
            <div class="col-xs-1 text-right">
              <label for="">Bill No. :</label>
            </div>
            <div class="col-xs-3 padT5">
              <select class="form-control chosen" name="bill_id" id="bill_id" style="width: 100%;">
                <option value="-1">---Select a bill----</option>
                @foreach(BillTable::distinct('bill')->lists('bill') as $bill)
                <option value="{{$bill}}">{{$bill}}</option>
                @endforeach

              </select>
            </div>

            <div class="col-xs-2 text-right">
              <label for="">Bill Date:</label>
            </div>
            <div class="col-xs-3">
              <input type="text" class="form-control" name="bill_date" id="bill_date" value="" readonly="true" />
            </div>
          </div>
        </div><!-- /.box-body -->
      </div>

      <div class="box box-warning mrgB20">
        <div class="box-header">
          <h3 class="box-title product-num">Product Details</h3>
        </div>
        <div class="box-body">
          <!-- single product -->
          <div class="row mrgB20 single-product">
            <div class="col-xs-1 text-right">
              <label for="">Code:</label>
            </div>
            <div class="col-xs-3">
              <input type="text" class="form-control" name="code" id="code" value="" readonly="true" />
            </div>

            <div class="col-xs-2 text-right">
              <label for="">Sold (pieces):</label>
            </div>
            <div class="col-xs-2">
              <input type="number" class="form-control" min="0" name="sold" id="sold" value="" readonly="true" />
            </div>

            <div class="col-xs-2 text-right">
              <label for="">Return (pieces):</label>
            </div>
            <div class="col-xs-2">
              <input type="number" class="form-control" min="0" name="return" id="return" value="0"  />
            </div>
          </div>
          <!-- Single product end -->

        </div><!-- /.box-body -->
      </div>

      <div class="row mrgA20">
        <div class="form-row col-md-4 col-md-offset-8 ">
          <div class="form-label col-md-8">
            <input type="submit" value="Save" class="btn btn-primary medium wid100">
          </div>

          <a href="{{URL::to('/')}}" class="btn medium bg-gray col-md-4" title="">
            <span class="button-content"> Cancel</span>
          </a>
        </div>
      </div>

    </form>
  </section><!-- /.content -->



</div><!-- /.content-wrapper -->
<!-- MAIN CONTENT ENDS -->
@stop



@section('custom_script')

<script>
  function validate()
  {
    var check = $( "#bill_id" ).val();
    console.log(check);
    if(check == -1)
    {
      alert('Please select a bill');

      return false;
    }

    return true;
  }

  jQuery(document).ready(function() {

    $(".chosen").chosen();

  });


  $(document).ready(function(){

    function doIt(){
      var sum = 0;

      $('.single-product').each(function(k,v){

        if(k>0){

          var ret = document.getElementById('returnsft'+k).value;
                    //console.log("return="+ret);

                    var rate = document.getElementById('rate'+k).value;
                    //console.log("rate="+rate);

                    sum+= parseFloat(ret)*parseFloat(rate);
                  }

                });
      if(parseInt(($('#oldDue').text()) - sum)<0){
        $('#back').val(sum- parseInt($('#oldDue').text()));
        $('#due').val(0);
      }else{
        $('#due').val(parseInt(($('#oldDue').text()) - sum));
        $('#back').val(0);
      }

    }

    $('#bill_id').on('change',function(e){

      e.preventDefault();

      var check = $( "#bill_id" ).val();
        if(check == -1) //No option selection checking
        {
          $('#bill_date').val("");
          $('.single-product').slice(1).remove();
          $('.single-product:first').show();
          return;
        }


        var value = $( "#bill_id option:selected" ).text();

        var billId = parseInt(value);

        var bill = {id:billId};

        $.post('ajax-returns',bill, function(data) //set the link here for ajax request
        {
          var data = JSON.parse(data);
          console.log(data);
          var b_date = data['date'];
          $('#bill_date').val(b_date);

          console.log(data['products'].length);

          var totalproduct = 0;

          $('.single-product').slice(1).remove();
          $first = $('.single-product:first');

          $.each(data['products'],function(k,v){
            $first.show();
            $('.single-product:last').clone(true).insertAfter(".single-product:last");
            ++totalproduct;
            $div = $('.single-product:last');
            $div.find("*[name]").each(function() { $(this).attr("name", $(this).attr("name").replace(/\d/g, "") + totalproduct); });
            $div.find("*[id]").each(function() { $(this).attr("id", $(this).attr("id").replace(/\d/g, "") + totalproduct); });


                //console.log(data['products'][k]['product_code']);

                $('#code'+totalproduct).val(data['products'][k]['product_code']);//product code
                $('#sold'+totalproduct).val(data['products'][k]['sold_piece']); //sold pieces

              });
          $first.hide();

        })
});
});


</script>

@stop