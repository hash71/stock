<script>
  jQuery(document).ready(function($) {

      $('input[type=submit]').click(function(event) {
            //fill empty number fields with 0
            $('input[type=number]').each(function(){

                if($(this).val()=="") //check for NaN
                    $(this).val(0);
            });
        });



      //Datepicker
      $('.datepicker').datepicker({
        'format': 'yyyy-mm-dd'
      });

      //Adding product and removing product script
      var totalproduct = 1;

      $('#newproduct').click(function(){
        //selecting the product_div clone it and insert it after the last product div
        $('.single-product:last').clone(true).insertAfter(".single-product:last");

        ++totalproduct;

        var intext = "Product "+totalproduct;
        //console.log(intext);
        $div = $('.single-product:last');
        $div.find('.product-num').html(intext);

        $div.find("*[name]").each(function() {
          $(this).attr("name", $(this).attr("name").replace(/\d/g, "") + totalproduct);
        });

        $div.find("*[id]").each(function() {
          $(this).attr("id", $(this).attr("id").replace(/\d/g, "") + totalproduct);
        });

        //Setting newly added field input value 0
        $(".single-product:last input[type=number]").each(function(){
          $(this).val("");
        });
        $(".single-product:last input[type=text]").each(function(){
          $(this).val("");
        });
        $('#asize'+totalproduct).val("");
        $('#bsize'+totalproduct).val("");

        $(".single-product:last #autocomplete"+totalproduct).val("");

        autocompletefunc($('#autocomplete'+totalproduct));

      });

      //product remove
      $('.remove').click(function(event){

        //If there is only one product then that product can't be deleted
        if(totalproduct<=1)
        {
          alert("Only one product can't be deleted");
          return ;
        }
        var elemid = $(this).attr("id");
        var par = $('#'+elemid).closest('.single-product');
        par.remove();
        --totalproduct;

        //update other products
        var productcount = 1;

        $('.single-product').each(function() {

          var intext = "Product "+productcount;

          $(this).find('.product-num').html(intext);

          $(this).find("*[name]").each(function() {
            $(this).attr("name", $(this).attr("name").replace(/\d/g, "") + productcount);
          });

          $(this).find("*[id]").each(function() {
            $(this).attr("id", $(this).attr("id").replace(/\d/g, "") + productcount);
          });

          productcount++;
        });

      });

    });
</script>