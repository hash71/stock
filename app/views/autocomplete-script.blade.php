<script>
var autocompletefunc = null;
  jQuery(document).ready(function($) {

        autocompletefunc = function(elem){
          //console.log(elem);
        var prodlist=[
        <?php
        foreach ($products as $pc)
        {
            echo '"'.$pc.'",';
        }
        ?>

        ];

        elem.autocomplete({
            autoFocus:true,
            source: function(request, response) {
                var results = $.ui.autocomplete.filter(prodlist, request.term);

                response(results.slice(0, 5));
            }

        });
    }

    autocompletefunc($('#autocomplete1'));


      //Datepicker
      $('.datepicker').datepicker({
        'format': 'yyyy-mm-dd'
      });

      //Adding product and removing product script
      var totalproduct = 1;

      $(document).on('click','#newproduct',function(){
        //selecting the product_div clone it and insert it after the last product div
        $('.single-product:last').clone().insertAfter(".single-product:last");

        ++totalproduct;

        var intext = "Product "+totalproduct;
        //console.log(intext);
        $div = $('.single-product:last');
        $div.find('.product-num').html(intext);

        $div.find("*[name]").each(function() {
          $(this).attr("name", $(this).attr("name").replace(/\d/g, "") + totalproduct);
        });

        $div.find("*[id]").each(function() {
          $(this).attr("id", $(this).attr("id").replace(/\d/g, "") + totalproduct);
        });

        //Setting newly added field input value 0
        $(".single-product:last input[type=number]").each(function(){
          $(this).val("");
        });
        $(".single-product:last input[type=text]").each(function(){
          $(this).val("");
        });
        $('#asize'+totalproduct).val("");
        $('#bsize'+totalproduct).val("");

        $(".single-product:last #autocomplete"+totalproduct).val("");

        autocompletefunc($('#autocomplete'+totalproduct));

      });

      //product remove
      $(document).on('click','.remove',function(event){

        //If there is only one product then that product can't be deleted
        if(totalproduct<=1)
        {
          alert("Only one product can't be deleted");
          return ;
        }
        var elemid = $(this).attr("id");
        var par = $('#'+elemid).closest('.single-product');
        par.remove();
        --totalproduct;

        //update other products
        var productcount = 1;

        $('.single-product').each(function() {

          var intext = "Product "+productcount;

          $(this).find('.product-num').html(intext);

          $(this).find("*[name]").each(function() {
            $(this).attr("name", $(this).attr("name").replace(/\d/g, "") + productcount);
          });

          $(this).find("*[id]").each(function() {
            $(this).attr("id", $(this).attr("id").replace(/\d/g, "") + productcount);
          });

          productcount++;
        });

      });

    });
</script>