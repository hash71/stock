<!-- FOOTER SCRIPTS STARTS -->
<!-- jQuery 2.1.3 -->
{{HTML::script('assets/js/jquery-1.11.2.min.js')}}
<!-- Bootstrap 3.3.2 JS -->
{{HTML::script('bootstrap/js/bootstrap.min.js')}}
<!-- SlimScroll -->
{{HTML::script('assets/js/jquery.slimScroll.min.js')}}
<!-- FastClick -->
{{HTML::script('assets/js/fastclick.min.js')}}
<!-- AdminLTE App -->
{{HTML::script('assets/js/app.min.js')}}
{{HTML::script('assets/js/bootstrap-datepicker.js')}}
{{HTML::script('assets/js/chosen.jquery.min.js')}}
{{HTML::script('assets/js/custom.js')}}
{{HTML::script('assets/js/jquery-ui.min.js')}}

<!-- FOOTER SCRIPTS ENDS -->