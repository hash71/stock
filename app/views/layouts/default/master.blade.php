<!DOCTYPE html>
<html>
@include('layouts.default.metaHead')


<body class="skin-green">

  <!-- Site wrapper -->
  <div class="wrapper">

    @include('layouts.default.header')

    @include('layouts.default.sidebar')

    @yield('content')

    @include('layouts.default.footer')

  </div><!-- ./wrapper -->

  @include('layouts.default.scripts')

  @yield('custom_script')

</body>

</html>