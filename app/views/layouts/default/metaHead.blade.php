<!-- METAHEAD STARTS -->
<head>
  <meta charset="UTF-8">
  <title>Mahbub Mosaic | Dashboard</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.3.2 -->
  {{HTML::style('bootstrap/css/bootstrap.min.css')}}
  <!-- Font Awesome Icons -->
  {{HTML::style('assets/css/font-awesome.min.css')}}
  <!-- Theme style -->
  {{HTML::style('assets/css/AdminLTE.min.css')}}
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  {{HTML::style('assets/css/skins/skin-green.min.css')}}
  {{HTML::style('assets/css/style.css')}}
  {{HTML::style('assets/css/chosen.min.css')}}
  {{HTML::style('assets/css/jquery-ui.min.css')}}
  {{HTML::style('assets/css/jquery-ui.structure.min.css')}}
  {{HTML::style('assets/css/jquery-ui.theme.min.css')}}
  {{HTML::style('assets/js/vendor/datatables/css/dataTables.bootstrap.css')}}
  {{HTML::style('assets/js/vendor/datatables/css/ColVis.css')}}
  {{HTML::style('assets/js/vendor/datatables/css/TableTools.css')}}
  {{HTML::style('assets/css/datepicker.css')}}

</head>
<!-- METAHEAD ENDS -->