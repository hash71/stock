<!-- HEADER STARTS -->
<header class="main-header">
  <a href="{{URL::to('/')}}" class="logo"><b>Mahbub</b> Mosaic</a>
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    <div style="position: absolute; margin-left: 40%;">
      <h4 style="margin-top: 15px; color: #fff;">@yield('page-header')</h4>
    </div>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">

        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">

            <span class="hidden-xs">{{Auth::user()->username}}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="{{URL::to('assets/img/user.png')}}" alt="User Image">
              <p>
                {{Auth::user()->username}}
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div>   
                <a href="{{URL::to(url().'/users/logout')}}" class="btn btn-default btn-flat wid100">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
    <!-- HEADER ENDS -->