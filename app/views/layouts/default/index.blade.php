@extends('layouts.default.master')
@section('content')
<!-- MAIN CONTENT STARTS -->
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-body text-center" style="min-height: 400px;">
        <h2>Welcome to Stock Management system</h2><br>
        <h4>Browse the available options from the left sidebar</h4>
      </div><!-- /.box-body -->

      <div class="box-footer text-center">
        <p style="font-size: 16px; font-weight: 600;">Developed &amp; maintained by <a href="http://megaminds.co"><strong>Megaminds</strong></a> (Web &amp; IT solutions)</p>
        Contact: 01921099556, 01710340450<br/>
        Email: megamindscobd@gmail.com

      </div><!-- /.box-footer-->
    </div><!-- /.box -->

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- MAIN CONTENT ENDS -->
@stop