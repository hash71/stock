<!-- SIDEBAR STARTS -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="treeview">
        <a href="{{URL::to('stock/form/in')}} ">
          <i class="fa fa-dashboard"></i> <span>Create New LC</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>
      <li class="treeview">
        <a href="{{URL::to('stock/form/out')}} ">
          <i class="fa fa-dashboard"></i> <span>Create New Bill</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>
      <li class="treeview">
        <a href="{{URL::to('tables/lc-table')}} ">
          <i class="fa fa-dashboard"></i> <span>LC-wise Report</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>
      <li class="treeview">
        <a href="{{URL::to('tables/bill-table')}} ">
          <i class="fa fa-dashboard"></i> <span>Bill-wise Report</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>
      <li class="treeview">
        <a href="{{URL::to('tables/product-table')}} ">
          <i class="fa fa-dashboard"></i> <span>Product-wise Report</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>
      <li class="treeview">
        <a href="{{URL::to('products/wastage')}} ">
          <i class="fa fa-dashboard"></i> <span>Product Wastage</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>
      <li class="treeview">
        <a href="{{URL::to('products/returns')}} ">
          <i class="fa fa-dashboard"></i> <span>Product Return</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>
      <li class="treeview">
        <a href="{{URL::to('products/sample')}} ">
          <i class="fa fa-dashboard"></i> <span>Product Sample</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>
      <li class="treeview">
        <a href="{{URL::to('stock/summary')}} ">
          <i class="fa fa-dashboard"></i> <span>Stock Summary</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>
      <li class="treeview">
        <a href="{{URL::to('stock/monthly')}} ">
          <i class="fa fa-dashboard"></i> <span>Monthly Stock Sheet</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>
      <li class="treeview">
        <a href="{{URL::to('stock/daily-stock')}} ">
          <i class="fa fa-dashboard"></i> <span>Daily Stock Sheet</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>
      <li class="treeview">
        <a href="{{URL::to('products/statement')}} ">
          <i class="fa fa-dashboard"></i> <span>Product Statement</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
      </li>





    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

    <!-- SIDEBAR ENDS -->