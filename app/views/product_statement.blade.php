@extends('layouts.default.master')

@section('page-header', "Product Statement")

@section('content')


<!-- MAIN CONTENT STARTS -->
<div class="content-wrapper">
    <div class="row">
      <div class="text-right padA10">
        <button href="#" id="print" class="btn btn-primary medium mrgR20" title="">
        <span class="button-content"><i class="glyph-icon icon-list"></i> Print Product Statement</span>
        </button>
      </div>
    </div>
    <!-- tile body -->
    <section class="content">
      <form action="" method="post">
      <div class="box box-primary filter-box">
        <div class="box-header">
          <h3 class="box-title">Filter options</h3>
        </div>
        <div class="box-body">
          <div class="row">

            <div class="col-xs-1 text-right">
              <label for="">Product:</label>
            </div>
            <div class="col-xs-3 form-group padT5">
              <select name="product" id="product" class="form-control chosen">
                <option value="-1">--Select a product--</option>
                @foreach(ProductTable::distinct('code')->lists('code') as $code)
                  <option value="{{$code}}" <?php if($code == $scode) echo "selected"?> >{{$code}}</option>
                @endforeach
              </select>
            </div>

            <div class="col-xs-1">
              <input type="submit" class="btn btn-primary" value="Submit">
            </div>
          </div>
        </div><!-- /.box-body -->
      </div>
      </form>

      <div class="box">
        <div class="box-header text-center"><h4><strong>Product In</strong></h4></div>
        <div class="box-body">
          <table class="table table-striped">
            <tbody><tr>

              <th>LC No.</th>
              <th>Date</th>
              <th>Product Name</th>
              <th>Size</th>
              <th>Quantity(piece)</th>
              <th>Quantity(sft)</th>
            </tr>
            @if(isset($lc_data))
              @foreach($lc_data as $data)
              <tr>
                <td>{{$data->lc}}</td>
                <td>{{date("Y-m-d",strtotime($data->date))}}</td>
                <td>{{$data->product_code}}</td>
                <td>{{$size=$data->unit_product_size_first."X".$data->unit_product_size_second}}</td>
                <td>{{$data->piece}}</td>
                <td>{{$data->unit_product_size * $data->piece }}</td>
              </tr>
            @endforeach
            @endif



          </tbody></table>
        </div><!-- /.box-body -->
      </div>

      <div class="box">
        <div class="box-header text-center"><h4><strong>Product Sale</strong></h4></div>
        <div class="box-body">
          <table class="table table-striped">
            <tbody><tr>

              <th>Bill No.</th>
              <th>Chalan No.</th>
              <th>Date</th>
              <th>Product Name</th>
              <th>Size</th>
              <th>Quantity(piece)</th>
              <th>Quantity(sft)</th>
            </tr>

           @if(isset($bill_data))
              @foreach($bill_data as $data)
              <tr>
                <td>{{$data->bill}}</td>
                <td>{{$data->chalan}}</td>
                <td>{{$data->date}}</td>
                <td>{{$data->product_code}}</td>
                <td>{{$size}}</td>
                <td>{{$data->sold_piece}}</td>
                <td>{{$data->sold_sft}}</td>
              </tr>
            @endforeach
            @endif


          </tbody></table>
        </div><!-- /.box-body -->
      </div>

      <div class="box" style="width: 35%; margin-left: 30%;">
        <div class="box-header text-center"><h4><strong>Product Total</strong></h4></div>
        <div class="box-body">
          <table class="table table-striped">
            <tbody>
              <tr>
                <td>Product In:</td>
                <td>{{isset($total_in) ? $total_in : 0}}</td>
              </tr>
              <tr>
                <td>Product Sale:</td>
                <td>{{isset($total_sale) ? $total_sale : 0}} (Return : {{isset($total_return) ? $total_return : 0}})</td>
              </tr>
              <tr>
                <td>Product Wastage:</td>
                <td>{{isset($total_wastage) ? $total_wastage : 0}}</td>
              </tr>
              <tr>
                <td>Product Sample:</td>
                <td>{{isset($total_sample) ? $total_sample : 0}}</td>
              </tr>
            </tbody>
          </table>
        </div><!-- /.box-body -->
      </div>
    </section>
    <!-- /tile body -->
</div>
@stop
@section('custom_script')

<script>
  jQuery(document).ready(function($) {

    $('.chosen').chosen();

    //Datepicker
      $('.datepicker').datepicker({
        'format': 'yyyy-mm-dd'
      });

  });



</script>

@include('print-script')
@stop