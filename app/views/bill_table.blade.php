@extends('layouts.default.master')

@section('page-header', "Bill-wise Report")

@section('content')


<!-- MAIN CONTENT STARTS -->
<div class="content-wrapper">

    <div class="row">
      <div class="text-right padA10">
        <button href="#" id="print" class="btn btn-primary medium mrgR20" title="">
        <span class="button-content"><i class="glyph-icon icon-list"></i> Print Bill Report</span>
        </button>
      </div>
    </div>
    <!-- tile body -->
    <section class="content">
      <div class="box">
        <div class="box-body">
          <table  class="table table-datatable table-custom" id="advancedDataTable">
            <thead>
              <tr>
                <th class="sort-alpha">Date</th>
                <th class="sort-alpha">Bill</th>
                <th class="sort-alpha">Chalan</th>
                <th class="sort-alpha">Shop</th>
                <th class="sort-amount">Products</th>
                <th class="sort-numeric">Quantity<br>(piece)</th>
                <th class="sort-numeric">Quantity<br>(sft)</th>
              </tr>
            </thead>
            <tbody>
              <tr></tr>
              <tr></tr>
              <tr></tr>
              <tr></tr>
              <tr></tr>
              <tr></tr>
              <tr></tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>
    <!-- /tile body -->

</div>
@stop
@section('custom_script')

<script src="../assets/js/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="../assets/js/vendor/datatables/ColReorderWithResize.js"></script>
<script src="../assets/js/vendor/datatables/colvis/dataTables.colVis.min.js"></script>
<script src="../assets/js/vendor/datatables/tabletools/ZeroClipboard.js"></script>
<script src="../assets/js/vendor/datatables/tabletools/dataTables.tableTools.min.js"></script>
<script src="../assets/js/vendor/datatables/dataTables.bootstrap.js"></script>

<script>
  $(function(){
// Add custom class to pagination div
$.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate paging_bootstrap paging_custom';
$('div.dataTables_filter input').addClass('form-control');
$('div.dataTables_length select').addClass('form-control');

/****************************************************/
/**************** ADVANCED DATATABLE ****************/
/****************************************************/
var oTable04 = $('#advancedDataTable').dataTable({
  "sDom":
  "<'row'<'col-md-4'l><'col-md-4 text-center sm-left'T C><'col-md-4'f>r>"+
  "t"+
  "<'row'<'col-md-4 sm-center'i><'col-md-4'><'col-md-4 text-right sm-center'p>>",
  "oLanguage": {
    "sSearch": ""
  },
  "sAjaxSource":'bill',
  "bProcessing": true,
  "bServerSide": true,

  "oTableTools": {
    "sSwfPath": "../assets/js/vendor/datatables/tabletools/swf/copy_csv_xls_pdf.swf",
    "aButtons": [
    /*"copy",
    "print",*/
    {
      "sExtends":    "collection",
      "sButtonText": 'Save <span class="caret" />',
      "aButtons":    [ "csv"/*, "xls", "pdf" */]
    }
    ]
  },
  "fnInitComplete": function(oSettings, json) {
    $('.dataTables_filter input').attr("placeholder", "Search");
  },
  "oColVis": {
    "buttonText": '<i class="fa fa-eye"></i>'
  }
});
$('.ColVis_MasterButton').on('click', function(){
  var newtop = $('.ColVis_collection').position().top ;
  $('.ColVis_collection').addClass('dropdown-menu');
  $('.ColVis_collection>li>label').addClass('btn btn-default')
  $('.ColVis_collection').css('top', newtop + 'px');
});
$('.DTTT_button_collection').on('click', function(){
  var newtop = $('.DTTT_dropdown').position().top ;
  $('.DTTT_dropdown').css('top', newtop + 'px');
});

// Add custom class
$('div.dataTables_filter input').addClass('form-control');
$('div.dataTables_length select').addClass('form-control');
})
</script>

@include('print-script')
@stop