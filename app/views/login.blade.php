<!DOCTYPE html>
<html>
<!-- METAHEAD STARTS -->
<head>
  <meta charset="UTF-8">
  <title>Mahbub Mosaic | Dashboard</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.3.2 -->
  <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- Font Awesome Icons -->
  <link href="../assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

  <!-- Theme style -->
  <link href="../assets/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link href="../assets/css/skins/skin-green.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/css/datepicker.css" rel="stylesheet" type="text/css" />
  <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
  <link href="../assets/css/login.css" rel="stylesheet" type="text/css" />
</head>
<!-- METAHEAD ENDS -->

  <body>
    <div class="login-box">
      <div class="login-box-body">
        <div class="login-logo">
        <a href="">Mahbub Mosaic</a> 
      </div><!-- /.login-logo -->
        <form action="{{URL::to('users/login')}}" method="post">
          <div class="form-group has-feedback">
            <input name="username" type="text" class="form-control" placeholder="Username"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="password" type="password" class="form-control" placeholder="Password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-4 col-xs-offset-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

  <!-- FOOTER SCRIPTS STARTS -->
  <!-- jQuery 2.1.3 -->
  <script src="../assets/js/jquery-1.11.2.min.js"></script>
  <!-- Bootstrap 3.3.2 JS -->
  <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <!-- SlimScroll -->
  <script src="../assets/js/jquery.slimScroll.min.js" type="text/javascript"></script>
  <!-- FastClick -->
  <script src='../assets/js/fastclick.min.js'></script>
  <!-- AdminLTE App -->
  <script src="../assets/js/app.min.js" type="text/javascript"></script>

  <script src="../assets/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <!-- FOOTER SCRIPTS ENDS -->

  </body>
</html>