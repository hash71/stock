@extends('layouts.default.master')

@section('page-header', "Product Wastage")

@section('content')
<!-- MAIN CONTENT STARTS -->
    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content">
        <!-- Default box -->
        <form action="{{URL::to('products/wastage')}}" method="post">

          <div class="box box-warning single-product mrgB20">
            <div class="box-header">
              <h3 class="box-title product-num">Product Details</h3>

            </div>
            <div class="box-body">
              <div class="row mrgB20">
                <div class="col-xs-1 text-right">
                <label for="">Date:</label>
                </div>
                <div class="col-xs-3">
                  <input type="text" class="form-control datepicker" name="date" id="date" value="{{date("Y-m-d")}}" required />
                </div>
                <div class="col-xs-1 text-right">
                  <label for="">Code:</label>
                </div>
                <div class="col-xs-3">
                  <input type="text" class="form-control" name="product_code" id="autocomplete1" value="" required />
                </div>

                <div class="col-xs-2 text-right">
                  <label for="">Wastage (pieces):</label>
                </div>
                <div class="col-xs-2">
                  <input type="number" class="form-control" min="0" name="wastage" id="wastage" value="" required />
                </div>
              </div>
            </div><!-- /.box-body -->
          </div>

          <div class="row mrgA20">
            <div class="form-row col-md-4 col-md-offset-8 ">
              <div class="form-label col-md-8">
                <input type="submit" value="Save" class="btn btn-primary medium wid100">
              </div>

              <a href="{{URL::to('/')}}" class="btn medium bg-gray col-md-4" title="">
                <span class="button-content"> Cancel</span>
              </a>
            </div>
          </div>

        </form>
      </section><!-- /.content -->



    </div><!-- /.content-wrapper -->
    <!-- MAIN CONTENT ENDS -->
    @stop



@section('custom_script')

<script type="text/javascript">

var autocompletefunc = null;
jQuery(document).ready(function($) {
      //Datepicker
      $('.datepicker').datepicker({
        'format': 'yyyy-mm-dd'
      });

        autocompletefunc = function(elem){
          console.log(elem);
        var prodlist=[
        <?php
        foreach ($products as $pc)
        {
            echo '"'.$pc.'",';
        }
        ?>

        ];

        elem.autocomplete({
            autoFocus:true,
            source: function(request, response) {
                var results = $.ui.autocomplete.filter(prodlist, request.term);

                response(results.slice(0, 5));
            }

        });
    }

    autocompletefunc($('#autocomplete1'));

});


</script>

@stop