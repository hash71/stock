@extends('layouts.default.master')

@section('page-header', "Daily Stock Sheet")

@section('content')


<!-- MAIN CONTENT STARTS -->
<div class="content-wrapper">
    <div class="row">
      <div class="text-right padA10">
        <button href="#" id="print" class="btn btn-primary medium mrgR20" title="">
        <span class="button-content"><i class="glyph-icon icon-list"></i> Print Daily Stock Sheet</span>
        </button>
      </div>
    </div>
    <!-- tile body -->
    <section class="content">
      <div class="box">
        <div class="box-body">
          <table class="table table-striped">
            <tbody><tr>
              <th>Product Code</th>
              <!-- <th>Dimension</th> -->
              <th>Unit Size</th>
              <th>Pieces<br>(Opening)</th>
              <th>Sold today<br>(Pieces)</th>
              <th>Pieces<br>(Closing)</th>
              <th>Stock (sft)<br>(Opening)</th>
              <th>Sold today<br> (sft)</th>
              <th>Stock (sft) <br> (Closing)</th>
              <th>Wastage </th>
              <th>Sample </th>
            </tr>
            @foreach($codes as $code)

              <tr>
                <td>{{$code}}</td>

                <td>{{ProductTable::where('code',$code)->pluck('dimension')}}</td>

                <!-- <td>{{ProductTable::where('code',$code)->pluck('unit_size')}}</td> -->

                <td>{{

                  $opening =

                  ProductTable::where('code',$code)->pluck('now_piece')
                  +
                  StockOut::where('product_code',$code)->where('date',date("Y-m-d"))->sum('sold_piece')
                  +
                  WSR::where('product_code',$code)->where('date',date("Y-m-d"))->where('type','sample')->sum('piece')
                  +
                  WSR::where('product_code',$code)->where('date',date("Y-m-d"))->where('type','wastage')->sum('piece')

                  }}</td>

                <td>{{$sold = StockOut::where('product_code',$code)->where('date',date("Y-m-d"))->sum('sold_piece')}}</td>

                <td>{{$closing = ProductTable::where('code',$code)->pluck('now_piece')}}</td>

                <td>{{$opening * ProductTable::where('code',$code)->pluck('unit_size')}}</td>

                <td>{{$sold * ProductTable::where('code',$code)->pluck('unit_size')}}</td>

                <td>{{$closing * ProductTable::where('code',$code)->pluck('unit_size')}}</td>

                <td>{{WSR::where('product_code',$code)->where('date',date("Y-m-d"))->where('type','wastage')->sum('piece') ? WSR::where('product_code',$code)->where('date',date("Y-m-d"))->where('type','wastage')->sum('piece') : 0}}</td>

                <td>{{WSR::where('product_code',$code)->where('date',date("Y-m-d"))->where('type','sample')->sum('piece') ? WSR::where('product_code',$code)->where('date',date("Y-m-d"))->where('type','sample')->sum('piece') : 0}}</td>

              </tr>

            @endforeach


          </tbody></table>
        </div><!-- /.box-body -->
      </div>
    </section>
    <!-- /tile body -->
</div>
@stop
@section('custom_script')

@include('print-script')

@stop