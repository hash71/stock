@extends('layouts.default.master')

@section('page-header', "Product Sale")

@section('content')
<!-- MAIN CONTENT STARTS -->
    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content">
        <!-- Default box -->
        <form action="{{URL::to('stock/store-stockout')}}" method="post">
          <div class="box box-primary" style="margin-bottom: 50px;">
            <div class="box-header">
              <h3 class="box-title">Bill information</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-1 text-right">
                  <label for="">Date :</label>
                </div>
                <div class="col-xs-2">
                  <input type="text" class="form-control datepicker" placeholder="YYYY-MM-DD" id="lcdate" name="date" value="{{date("Y-m-d")}}" required />
                </div>
                <div class="col-xs-1 text-right">
                  <label for="">Bill No. :</label>
                </div>
                <div class="col-xs-2">
                  <input type="text" name="bill" class="form-control" id="billnumber" value="" required />
                </div>
                <div class="col-xs-1 text-right">
                  <label for="">Chalan :</label>
                </div>
                <div class="col-xs-2">
                  <input type="text" name="chalan" class="form-control" id="chalannumber" value="" required />
                </div>
                <div class="col-xs-1">
                  <label for="">Shop :</label>
                </div>
                <div class="col-xs-2 form-group">
                  <select name="shop" id="shop" class="form-control">
                    <option value="shop1">shop1</option>
                    <option value="shop2">shop2</option>
                    <option value="shop3">shop3</option>
                  </select>
                </div>
              </div>
            </div><!-- /.box-body -->
          </div>

          <div class="box box-warning single-product mrgB20">
            <div class="box-header">
              <h3 class="box-title product-num">Product 1</h3>
              <a href="#" class="remove btn btn-sm bg-red pull-right tooltip-button mrgR10" data-placement="left" title="" data-original-title="Remove Product" style="margin-top:2px;" id="remove1"><i class="fa fa-remove"></i></a>
            </div>
            <div class="box-body">
              <div class="row mrgB20">
                <div class="col-xs-1 text-right">
                  <label for="">Code:</label>
                </div>
                <div class="col-xs-3">
                  <input type="text" class="form-control productajax" name="product_code1" id="autocomplete1" value="" required />
                </div>

                <input type="hidden" class="productsize" name="size1" id="size1" value="">

                <div class="col-xs-2 text-right">
                  <label for="">Quantity (pieces):</label>
                </div>
                <div class="col-xs-2">
                  <input type="number" class="form-control productpiece" min="0" name="piece1" id="quantity1" value="" required />
                </div>

                <div class="col-xs-2 text-right">
                  <label for="">Quantity (sft):</label>
                </div>
                <div class="col-xs-2">
                  <input type="number" class="form-control productsft" min="0" name="sft1" id="sft1" value="" readonly="true" />
                </div>

              </div>
            </div><!-- /.box-body -->
          </div>

          <div class="row mrgA20">
            <a href="javascript:;" class="btn medium bg-aqua col-md-2" title="" id="newproduct">
              <span class="button-content"><i class="fa fa-plus"></i> Add another product</span>
            </a>
            <div class="form-row col-md-4 col-md-offset-6 ">
              <div class="form-label col-md-8">
                <input type="submit" value="Save Bill" class="btn btn-primary medium wid100">
              </div>

              <a href="" class="btn medium bg-gray col-md-4" title="">
                <span class="button-content"> Cancel</span>
              </a>
            </div>
          </div>

        </form>
      </section><!-- /.content -->



    </div><!-- /.content-wrapper -->
    <!-- MAIN CONTENT ENDS -->
    @stop



@section('custom_script')

@include('autocomplete-script')


<script>

jQuery(document).ready(function($) {

  $(document).on('blur', '.productajax', function(event) {

        $div = $(this);

        var productname = $div.val();

        $.ajax({
          type: "POST",
          url: '../ajaxstockout',
          data: { product_code : productname }
        }).success(function( result ) {
            //work with the result
            console.log(result);

            $div.closest('.single-product').find('.productsize').val(result);

            var piece = $div.closest('.single-product').find('.productpiece').val();

            if(piece != "")
            {
               var totsft = piece*result;
               $div.closest('.single-product').find('.productsft').val(totsft.toFixed(2));
            }

          });
  });


  $(document).on('keyup','.productpiece',function(event) {

      var val = $(this).val();

      var size = $(this).closest('.single-product').find('.productsize').val();

      var totsft = val*size;
      $(this).closest('.single-product').find('.productsft').val(totsft.toFixed(2));
  });


  $(document).on('blur', '#billnumber', function(event) {
        $div = $(this);

        var billid = $div.val();

        $.ajax({
          type: "POST",
          url: '../check-bill',
          data: { bill_id: billid }
        }).success(function( result ) {
            //work with the result
            if(result == "1")
              alert("Bill already exists");
        });
  });


});

</script

@stop