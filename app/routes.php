<?php

Route::get('test_user',function(){

	User::create(['username'=>'megaminds','password'=>Hash::make('mm')]);

});



Route::controller('users','UsersController');

Route::group(['before'=>'auth'], function(){

	Route::get('/',function(){

		return View::make('layouts.default.index');

	});

	Route::controller('products','ProductsController');

	Route::controller('stock','StockController');

	Route::controller('tables','TableController');
});







