<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class LcTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$products = 'x'.'<br>'.'y'.'<br>';

		$piece = '1'.'<br>'.'2'.'<br>';

		$size = '100'.'<br>'.'200'.'<br>';

		foreach(range(1, 100) as $index){

			$model = new LcTable;

			$model->lc = $faker->username;

			$model->date = $faker->dateTime($max = 'now');

			$model->product = $products;

			$model->piece = $piece;

			$model->size = $size;

			$model->save();

		}

	}

}