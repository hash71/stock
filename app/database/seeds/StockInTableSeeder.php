<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
// use Faker\Provider\Internet as FakerInternet;

class StockInTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		// $faker = FakerInternet::create();

		foreach(range(1, 10) as $index)
		{	

			$rand = $faker->numberBetween($min = 1, $max = 5);

			$lc = $faker->username;

			$date = $faker->dateTime($max = 'now');


			for($i=1; $i<=$rand; $i++){

				$product_code_first = $faker->userName;
				$product_code_second = $faker->randomElement($array = array ('L','D','F','K'));
				$unit_product_size_first = $faker->randomNumber($nbDigits = 3);
				$unit_product_size_second = $faker->randomNumber($nbDigits = 2);


				StockIn::create([

					'lc' => $lc,
					'date'=>$date,
					'product_code'=> $faker->userName,				
					'product_code_first'=>$product_code_first,
					'product_code_second'=>$product_code_second,
					'product_code'=>$product_code_first.'('.$product_code_second.')',
					'piece'=>$faker->randomNumber($nbDigits = 4),
					'unit_product_size_first' => $unit_product_size_first,
					'unit_product_size_second' => $unit_product_size_second,
					'unit_product_size' => $unit_product_size_first * $unit_product_size_second,
					'wastage_piece'=>$faker->randomNumber($nbDigits = 2)				          

				]);
			}			
		}
	}
}