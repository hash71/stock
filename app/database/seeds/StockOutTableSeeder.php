<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class StockOutTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{	

			$rand = $faker->numberBetween($min = 1, $max = 3);

			$bill = $faker->username;

			$chalan = $faker->username;

			$date = $faker->dateTime($max = 'now');

			$product_code = StockIn::distinct('product_code')->lists('product_code');

			for($i=1; $i<=$rand; $i++){

				$model = new StockOut;
				$model->bill = $bill;
				$model->chalan = $chalan;
				$model->date = $date;
				$model->product_code = $faker->randomElement($product_code);
				$model->sold_piece = $faker->randomNumber($nbDigits = 4);
				$model->sold_sft = $faker->randomNumber($nbDigits = 4);
				$model->save();
					
			}
			

		}
	}

}