<?php

class StockController extends BaseController {

	public function getForm($type){

		if($type == 'in'){

			return View::make('stock_in');

		}else if($type == 'out'){

			$products = ProductTable::distinct('code')->lists('code');

			return View::make('stock_out',compact('products'));
		}

	}

	public function postStoreStockin(){

		$i = 1;

		while(Input::get('product_code_first'.$i)){

			$model = new StockIn;

			$model->lc = Input::get('lc');

			$model->date = Input::get('date');

			$model->product_code_first = Input::get('product_code_first'.$i);

			$model->product_code_second = Input::get('product_code_second'.$i);

			$model->product_code = $model->product_code_first.'('.$model->product_code_second.')';

			$model->piece = Input::get('piece'.$i);

			$model->unit_product_size_first = Input::get('unit_product_size_first'.$i);

			$model->unit_product_size_second = Input::get('unit_product_size_second'.$i);

			$model->unit_product_size = ($model->unit_product_size_first*$model->unit_product_size_second)/144;

			$model->wastage_piece = Input::get('wastage_piece'.$i);

			$model->save();

			$i++;

		}

		$this->lcTable();

		$this->productTableIn();

		//return View::make('stock_in');
		return Redirect::to('tables/lc-table');
	}

	public function postStoreStockout(){

		$i = 1;

		while (Input::get('product_code'.$i)) {

			$model = new Stockout;

			$model->bill = Input::get('bill');
			$model->chalan = Input::get('chalan');
			// $moedl->shop = Input::get('shop');
			$model->date = Input::get('date');
			$model->product_code = Input::get('product_code'.$i);
			$model->sold_piece = Input::get('piece'.$i);
			$model->sold_sft = Input::get('sft'.$i);

			$model->save();

			$i++;
		}

		$this->billTable();

		$this->productTableOut();

		//return View::make('stock_out');
		return Redirect::to('tables/bill-table');
	}

	public function postAjaxstockout(){

		//return "hello";

		return ProductTable::where('code',Input::get('product_code'))->pluck('unit_size');

	}

	public function postCheckBill(){

		$billid = Input::get('bill_id');

		$found = StockOut::where('bill',$billid)->first();

		return sizeof($found);
	}

	public function lcTable(){

		// LcTable::truncate();

		$model = new LcTable;

		$model->lc = Input::get('lc');

		$model->date = Input::get('date');

		$product = "";

		$piece = "";

		$size = "";

		$i = 1;

		while(Input::get('product_code_first'.$i)){

			$product .= Input::get('product_code_first'.$i).'<br>';
			$piece .= Input::get('piece'.$i).'<br>';
			$size .= Input::get('unit_product_size_first'.$i).'X'.Input::get('unit_product_size_second'.$i).'<br>';
			$i++;

		}
		$model->product = $product;
		$model->piece = $piece;
		$model->size = $size;

		// echo dd($model);

		$model->save();
	}

	public function billTable(){

		// BillTable::truncate();

		$model = new BillTable;

		$model->bill = Input::get('bill');
		$model->chalan = Input::get('chalan');
		$model->date = Input::get('date');

		$model->shop = Input::get('shop');

		$model->product = "";
		$model->piece = "";
		$model->size = "";

		$i = 1;

		while (Input::get('product_code'.$i)) {


			$model->product .= Input::get('product_code'.$i).'<br>';
			$model->piece .= Input::get('piece'.$i).'<br>';
			$model->size .= Input::get('sft'.$i).'<br>';

			$i++;

		}

		$model->save();

	}

	public function productTableIn(){

		$i = 1;

		while(Input::get('product_code_first'.$i)){


			$product_code_first = Input::get('product_code_first'.$i);

			$product_code_second = Input::get('product_code_second'.$i);

			$product_code = $product_code_first.'('.$product_code_second.')';

			if(sizeof(ProductTable::where('code',$product_code)->get())){

				$data = ProductTable::where('code',$product_code)->get();

				$data = $data[0];

				$in = $data->total_in + Input::get('piece'.$i);

				$wastage = $data->wastage + Input::get('wastage_piece'.$i);

				$now = $in - ($wastage + $data->sample + $data->total_out);

				ProductTable::where('code',$product_code)
							->update([
								'total_in'=>$in,
								'wastage'=>$wastage,
								'now_piece'=>$now,
								'now_sft'=>$now*$data->unit_size
							]);



			}else{



				$in = Input::get('piece'.$i);
				$wastage = Input::get('wastage_piece'.$i);
				$total_in = $in - $wastage;
				$unit = (Input::get('unit_product_size_first'.$i)*	Input::get('unit_product_size_second'.$i))/144;

				ProductTable::insert([

					'code'=>$product_code,
					'total_in'=>$in,
					'wastage'=>$wastage,
					'unit_product_size_first' => Input::get('unit_product_size_first'.$i),
					'unit_product_size_second' => Input::get('unit_product_size_second'.$i),
					'unit_size'=>$unit,
					'dimension'=>Input::get('unit_product_size_first'.$i)."X".Input::get('unit_product_size_second'.$i),
					'mother_code'=> Input::get('product_code_first'.$i),
					'now_piece'=> $total_in,
					'now_sft'=>$total_in*$unit

				]);

			}

			$i++;

		}



	}

	public function productTableOut(){

		$i=1;

		while (Input::get('product_code'.$i)) {

			$data = ProductTable::where('code',Input::get('product_code'.$i))->get();

			$data = $data[0];

			$n = $data->now_piece-(Input::get('piece'.$i));

			ProductTable::where('code',$data->code)
				->update([
					'total_out' => $data->total_out + Input::get('piece'.$i),
					'now_piece' => $n,
					'now_sft'=> $n * $data->unit_size
				]);
			$i++;
		}
	}


	public function getDailyStock(){

		$codes = Stockout::where('date',date("Y-m-d"))
				->distinct('product_code')
				->lists('product_code');
		return View::make('daily-stock',compact('codes'));

	}


	public function getMonthly(){

		$start = date("Y-m-01");
		$end = date("Y-m-t");

		$datas = DB::table('stock_out')
				->select('product_code',DB::raw('SUM(sold_piece) as sold_piece'))
				->groupBy('product_code')
				->where('date','<=',$end)
				->where('date','>=',$start)
				->get();


		$m = date("m");
		$y = date("Y");

		return View::make('monthly-sales',compact('datas','start','end','m','y'));
	}

	public function postMonthly(){

		$start = date(Input::get('year')."-".Input::get('month')."-"."01");
		$end = date(Input::get('year')."-".Input::get('month')."-"."t");



		$datas = DB::table('stock_out')
				->select('product_code',DB::raw('SUM(sold_piece) as sold_piece'))
				->groupBy('product_code')
				->where('date','<=',$end)
				->where('date','>=',$start)
				->get();

		$m = Input::get('month');
		$y = Input::get('year');

		return View::make('monthly-sales',compact('datas','start','end','m','y'));
	}

	public function getSummary(){

		$start = date("Y-m-d");
		$end = date("Y-m-d");
		$scode = "-1";




			$datas = DB::table('stock_out')
					->select('product_code',DB::raw('SUM(sold_piece) as sold_piece'))
					->groupBy('product_code')
					->where('date','<=',$end)
					->where('date','>=',$start)
					->get();


		return View::make('stock-summary',compact('datas','start','end','scode'));
	}


	public function postSummary(){

		$start = Input::get('from')?Input::get('from'):date("Y-m-d");
		$end = Input::get('to')?Input::get('to'):date("Y-m-d");
		$scode = Input::get('product');

		if(Input::get('product')!=-1){

			$datas = DB::table('stock_out')
				->select('product_code',DB::raw('SUM(sold_piece) as sold_piece'))
				->where('date','<=',$end)
				->where('date','>=',$start)
				->where('product_code',Input::get('product'))
				->get();
		}else{



			$datas = DB::table('stock_out')
					->select('product_code',DB::raw('SUM(sold_piece) as sold_piece'))
					->groupBy('product_code')
					->where('date','<=',$end)
					->where('date','>=',$start)
					->get();

		}
		// return dd(Input::all());

		return View::make('stock-summary',compact('datas','start','end','scode'));
	}
}
