<?php

class UsersController extends \BaseController {
	

	public function getLogin(){

		if(Auth::check()){

			return Redirect::to('/');
		}
		return View::make('login');
	}

	public function postLogin(){
		
		
		if(Auth::attempt(['username'=>Input::get('username'),'password'=>Input::get('password')])){
			
			return Redirect::to('/');
			
		}else{

			return Redirect::to('users/login');
		}

	}

	public function getLogout(){

		Auth::logout();

		return Redirect::to('users/login');

	}

}