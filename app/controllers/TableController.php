<?php

class TableController extends BaseController {


	public function getLcTable(){
		return View::make('lc_table');
	}

	public function getLc(){

		$cols = [
			'lc',
			'date',
			'product',
			'piece',
			'size'
		];

		Dtable::ajax('lc_table',$cols);


	}

	public function getBillTable(){
		return View::make('bill_table');
	}

	public function getBill(){

		$cols = [
			'date',
			'bill',
			'chalan',
			'shop',
			'product',
			'piece',
			'size'
		];

		Dtable::ajax('bill_table',$cols);
	}

	public function getProductTable(){
		return View::make('product_table');
	}

	public function getProduct(){

		$cols = [
			'code',
			'total_in',
			'total_out',
			'wastage',
			'sample',
			'returns',
			'dimension',
			'now_piece',
			'now_sft'

		];

		Dtable::ajax('product_table',$cols);
	}





}
