<?php

class ProductsController extends \BaseController {

	public function getWastage(){

		$products = ProductTable::distinct('code')->lists('code');

		return View::make('wastage',compact('products'));

	}
	public function getSample(){

		$products = ProductTable::distinct('code')->lists('code');

		return View::make('sample',compact('products'));

	}
	public function getReturns(){

		$products = ProductTable::distinct('code')->lists('code');

		return View::make('returns',compact('products'));

	}

	public function postSample(){

		$this->wsr('sample');

		$data = ProductTable::where('code',Input::get('product_code'))->get();

		$data = $data[0];
		$now = $data->now_piece - Input::get('sample');

		ProductTable::where('code',Input::get('product_code'))
			->update([
				'sample'=>Input::get('sample')+$data->sample,
				'now_piece'=>$now,
				'now_sft'=>$data->unit_size*$now
			]);

		return Redirect::to('tables/product-table');

	}

	public function postWastage(){

		$this->wsr('wastage');

		$data = ProductTable::where('code',Input::get('product_code'))->get();

		$data = $data[0];
		$now = $data->now_piece - Input::get('wastage');

		ProductTable::where('code',Input::get('product_code'))
			->update([
				'wastage'=>Input::get('wastage')+$data->wastage,
				'now_piece'=>$now,
				'now_sft'=>$data->unit_size*$now
			]);

		return Redirect::to('tables/product-table');
	}

	public function postReturns(){

		$this->wsr('return');

		$data = ProductTable::where('code',Input::get('product_code'))->get();

		$data = $data[0];

		$now = $data->now_piece + Input::get('return');

		ProductTable::where('code',Input::get('product_code'))
			->update([
				'returns'=>Input::get('return') + $data->returns,
				'now_piece'=>$now,
				'now_sft'=>$data->unit_size*$now,
				'total_out'=>$data->total_out - Input::get('return')
			]);

		return Redirect::to('tables/product-table');
	}

	public function postAjaxReturns(){

		$y = new stdClass();

		$x = StockOut::where('bill',Input::get('id'))
				->select('product_code','sold_piece')
				->get();

		$date = StockOut::where('bill',Input::get('id'))
					->pluck('date');
		$y->date = date("Y-m-d",strtotime($date));
		$y->products = $x;

		return json_encode($y);

	}

	public function getStatement(){

		$scode = "-1";


		$datas = [];


		return View::make('product_statement',compact('datas','scode'));
	}

	public function postStatement(){

		$scode = Input::get('product');

		$lc_data = StockIn::where('product_code',$scode)->get();

		$bill_data = StockOut::where('product_code',$scode)->get();

		$total_in = StockIn::where('product_code',$scode)->sum('piece');

		$total_wastage =  ProductTable::where('code',$scode)->sum('wastage');

		$total_sample = ProductTable::where('code',$scode)->sum('sample');

		$total_return = WSR::where('product_code',$scode)
						->where('type','return')
						->sum('piece');

		$total_sale = $total_in
					  -$total_wastage
					  -$total_sample
					  -ProductTable::where('code',$scode)->pluck('now_piece');

		return View::make('product_statement',compact('scode','lc_data','bill_data','total_in','total_wastage','total_sample','total_sale','total_return'));

	}

	public function wsr($type){//wastage sample return

		WSR::create([
			'product_code' => Input::get('product_code'),
			'piece' => Input::get($type),
			'date' => Input::get('date'),
			'type' => $type
		]);

	}

}